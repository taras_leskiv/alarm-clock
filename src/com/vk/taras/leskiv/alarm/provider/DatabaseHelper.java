package com.vk.taras.leskiv.alarm.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.vk.taras.leskiv.alarm.utils.Logger;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "alarms.db";
    private static final int DATABASE_VERSION = 2;

    private static final String SQL_CREATE = "CREATE TABLE alarms (" +
            "_id INTEGER PRIMARY KEY," +
            "hour INTEGER, " +
            "minutes INTEGER, " +
            "daysofweek INTEGER, " +
            "alarmtime INTEGER, " +
            "enabled INTEGER, " +
            "expanded INTEGER, " +
            "vibrate INTEGER, " +
            "message TEXT, " +
            "alert TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE);

        // insert default alarms
        String insertMe = "INSERT INTO alarms " +
                "(hour, minutes, daysofweek, alarmtime, enabled, expanded, vibrate, message, alert) " + "VALUES ";
        db.execSQL(insertMe + "(7, 0, 127, 0, 0, 0, 1, 'hello', '');");
        db.execSQL(insertMe + "(8, 30, 31, 0, 0, 0, 1, 'lol', '');");
        db.execSQL(insertMe + "(9, 00, 0, 0, 0, 0, 1, 'lol', '');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int currentVersion) {
        if (Logger.LOGV) Logger.v(
                "Upgrading alarms database from version " +
                        oldVersion + " to " + currentVersion +
                        ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS alarms");
        onCreate(db);
    }
}

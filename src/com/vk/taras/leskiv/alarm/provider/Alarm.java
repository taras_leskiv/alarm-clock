package com.vk.taras.leskiv.alarm.provider;

import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.vk.taras.leskiv.alarm.R;
import com.vk.taras.leskiv.alarm.utils.Logger;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public class Alarm implements Parcelable {

    // Parcelable APIs
    public static final Parcelable.Creator<Alarm> CREATOR =
            new Creator<Alarm>() {
                @Override
                public Alarm createFromParcel(Parcel parcel) {
                    return new Alarm(parcel);
                }

                @Override
                public Alarm[] newArray(int size) {
                    return new Alarm[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(id);
        parcel.writeInt(enabled ? 1 : 0);
        parcel.writeInt(hour);
        parcel.writeInt(minutes);
        parcel.writeInt(daysOfWeek.getCoded());
        parcel.writeLong(time);
        parcel.writeInt(vibrate ? 1 : 0);
        parcel.writeString(label);
        parcel.writeParcelable(alert, flags);
        parcel.writeInt(silent ? 1 : 0);
        parcel.writeInt(expanded ? 1 : 0);
    } // end Parcelable APId


    // Alarm fields
    public int id;
    public boolean enabled;
    public int hour;
    public int minutes;
    public DaysOfWeek daysOfWeek;
    public long time;
    public boolean vibrate;
    public String label;
    public Uri alert;
    public boolean silent;
    public boolean expanded;

    public Alarm(Cursor cursor) {
        id = cursor.getInt(AlarmColumns.ALARM_ID_INDEX);
        enabled = cursor.getInt(AlarmColumns.ALARM_ENABLED_INDEX) == 1;
        hour = cursor.getInt(AlarmColumns.ALARM_HOUR_INDEX);
        minutes = cursor.getInt(AlarmColumns.ALARM_MINUTES_INDEX);
        daysOfWeek = new DaysOfWeek(cursor.getInt(AlarmColumns.ALARM_DAYS_OF_WEEK_INDEX));
        time = cursor.getLong(AlarmColumns.ALARM_TIME_INDEX);
        vibrate = cursor.getInt(AlarmColumns.ALARM_VIBRATE_INDEX) == 1;
        label = cursor.getString(AlarmColumns.ALARM_MESSAGE_INDEX);
        String alertString = cursor.getString(AlarmColumns.ALARM_ALERT_INDEX);

        if (alertString.equals(Alarms.ALARM_ALERT_SILENT)) {
            if (Logger.LOGV) {
                Logger.v("Alarm is marked as silent");
            }
            silent = true;
        } else {
            if (alertString != null && alertString.length() != 0) {
                alert = Uri.parse(alertString);
            }

            // If the database alert is null or it failed to parse, use the default alert
            if (alert == null) {
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            }
        }

        expanded = cursor.getInt(AlarmColumns.ALARM_EXPANDED_INDEX) == 1;
    }

    public Alarm(Parcel p) {
        id = p.readInt();
        enabled = p.readInt() == 1;
        hour = p.readInt();
        minutes = p.readInt();
        daysOfWeek = new DaysOfWeek(p.readInt());
        time = p.readLong();
        vibrate = p.readInt() == 1;
        label = p.readString();
        alert = (Uri) p.readParcelable(null);
        silent = p.readInt() == 1;
        expanded = p.readInt() == 1;
    }

    public String getLabelOrDefault(Context context) {
        if (label == null || label.length() == 0) {
            return context.getString(R.string.default_label);
        }
        return label;
    }

    /*
     * Days of week code as a single int.
     * 0x00: no day
     * 0x01: Monday
     * 0x02: Tuesday
     * 0x04: Wednesday
     * 0x08: Thursday
     * 0x10: Friday
     * 0x20: Saturday
     * 0x40: Sunday
     */
    public static final class DaysOfWeek {
        public static final int MONDAY = 0;
        public static final int TUESDAY = 1;
        public static final int WEDNESDAY = 2;
        public static final int THUSRSDAY = 3;
        public static final int FRIDAY = 4;
        public static final int SATURDAY = 5;
        public static final int SUNDAY = 6;

        private static int[] DAY_MAP = new int[]{
                Calendar.MONDAY,
                Calendar.TUESDAY,
                Calendar.WEDNESDAY,
                Calendar.THURSDAY,
                Calendar.FRIDAY,
                Calendar.SATURDAY,
                Calendar.SUNDAY,
        };

        // Bitmask of all repeating days
        private int mDays;

        public DaysOfWeek(int days) {
            mDays = days;
        }

        public String toString(Context context, boolean showNever) {
            StringBuilder ret = new StringBuilder();

            // no days
            if (mDays == 0) {
                return showNever ?
                        context.getText(R.string.never).toString() : "";
            }

            // every day
            if (mDays == 0x7f) {
                return context.getText(R.string.every_day).toString();
            }

            // count selected days
            int dayCount = 0, days = mDays;
            while (days > 0) {
                if ((days & 1) == 1) dayCount++;
                days >>= 1;
            }

            // short or long form?
            DateFormatSymbols dfs = new DateFormatSymbols();
            String[] dayList = (dayCount > 1) ?
                    dfs.getShortWeekdays() :
                    dfs.getWeekdays();

            // selected days
            for (int i = 0; i < 7; i++) {
                if ((mDays & (1 << i)) != 0) {
                    ret.append(dayList[DAY_MAP[i]]);
                    dayCount -= 1;
                    if (dayCount > 0) ret.append(
                            context.getText(R.string.day_concat));
                }
            }
            return ret.toString();
        }

        public boolean isSet(int day) {
            return ((mDays & (1 << day)) > 0);
        }

        public void set(int day, boolean set) {
            if (set) {
                mDays |= (1 << day);
            } else {
                mDays &= ~(1 << day);
            }
        }

        public void set(DaysOfWeek dow) {
            mDays = dow.mDays;
        }

        public int getCoded() {
            return mDays;
        }

        // Returns days of week encoded in an array of booleans.
        public boolean[] getBooleanArray() {
            boolean[] ret = new boolean[7];
            for (int i = 0; i < 7; i++) {
                ret[i] = isSet(i);
            }
            return ret;
        }

        public boolean isRepeatSet() {
            return mDays != 0;
        }

        /**
         * returns number of days from today until next alarm
         *
         * @param c must be set to today
         */
        public int getNextAlarm(Calendar c) {
            if (mDays == 0) {
                return -1;
            }

            int today = (c.get(Calendar.DAY_OF_WEEK) + 5) % 7;

            int day = 0;
            int dayCount = 0;
            for (; dayCount < 7; dayCount++) {
                day = (today + dayCount) % 7;
                if (isSet(day)) {
                    break;
                }
            }
            return dayCount;
        }
    }
}

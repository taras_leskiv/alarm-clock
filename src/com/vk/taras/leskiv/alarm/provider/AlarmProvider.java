package com.vk.taras.leskiv.alarm.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.vk.taras.leskiv.alarm.utils.Logger;

public class AlarmProvider extends ContentProvider {
    private static final String AUTHORITY = "com.vk.taras.leskiv.alarm.provider";
    private static final String ALARMS_PATH = "alarms";

    public static final Uri ALARM_URI = Uri.parse("content://" + AUTHORITY + "/" + ALARMS_PATH);

    public static final String ALARMS_CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/vnd." + AUTHORITY + "." + ALARMS_PATH;
    public static final String ALARMS_CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +
            "/vnd." + AUTHORITY + "." + ALARMS_PATH;

    private SQLiteOpenHelper mOpenHelper;

    public static final int ALARMS = 1;
    public static final int ALARM_ID = 2;
    public static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AUTHORITY, ALARMS_PATH, ALARMS);
        sUriMatcher.addURI(AUTHORITY, ALARMS_PATH + "/#", ALARM_ID);
    }

    public AlarmProvider() {
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projectionIn, String selection,
                        String[] selectionArgs, String sort) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        // Generate the body of the query
        int match = sUriMatcher.match(uri);
        switch (match) {
            case ALARMS:
                qb.setTables(ALARMS_PATH);
                break;
            case ALARM_ID:
                qb.setTables(ALARMS_PATH);
                qb.appendWhere(AlarmColumns._ID + "=");
                qb.appendWhere(uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri" + uri);
        }

        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor cursor = qb.query(db, projectionIn, selection, selectionArgs,
                null, null,sort);

        if (cursor == null) {
            if (Logger.LOGV) Logger.v("Alarms query failed");
        } else {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case ALARMS:
                return ALARMS_CONTENT_TYPE;
            case ALARM_ID:
                return ALARMS_CONTENT_ITEM_TYPE;
        }
        throw new IllegalArgumentException("Unknown Uri" + uri);
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        if (sUriMatcher.match(uri) != ALARMS) {
            throw new IllegalArgumentException("Cannot insert into URL: " + uri);
        }

        ContentValues values;
        if (initialValues != null)
            values = new ContentValues(initialValues);
        else
            values = new ContentValues();

        if (!values.containsKey(AlarmColumns.HOUR))
            values.put(AlarmColumns.HOUR, 0);

        if (!values.containsKey(AlarmColumns.MINUTES))
            values.put(AlarmColumns.MINUTES, 0);

        if (!values.containsKey(AlarmColumns.DAYS_OF_WEEK))
            values.put(AlarmColumns.DAYS_OF_WEEK, 0);

        if (!values.containsKey(AlarmColumns.ALARM_TIME))
            values.put(AlarmColumns.ALARM_TIME, 0);

        if (!values.containsKey(AlarmColumns.ENABLED))
            values.put(AlarmColumns.ENABLED, 0);

        if (!values.containsKey(AlarmColumns.VIBRATE))
            values.put(AlarmColumns.VIBRATE, 1);

        if (!values.containsKey(AlarmColumns.MESSAGE))
            values.put(AlarmColumns.MESSAGE, "");

        if (!values.containsKey(AlarmColumns.ALERT))
            values.put(AlarmColumns.ALERT, "");

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert("alarms", AlarmColumns.MESSAGE, values);
        if (rowId < 0) {
            throw new SQLException("Failed to insert row into " + uri);
        }
        if (Logger.LOGV) Logger.v("Added alarm rowId = " + rowId);

        Uri newUrl = ContentUris.withAppendedId(ALARM_URI, rowId);
        getContext().getContentResolver().notifyChange(newUrl, null);
        return newUrl;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        long rowId = 0;
        switch (sUriMatcher.match(uri)) {
            case ALARMS:
                count = db.delete("alarms", where, whereArgs);
                break;
            case ALARM_ID:
                String segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);
                if (TextUtils.isEmpty(where)) {
                    where = "_id=" + segment;
                } else {
                    where = "_id=" + segment + " AND (" + where + ")";
                }
                count = db.delete("alarms", where, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Cannot delete from URL: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        int count;
        long rowId = 0;
        int match = sUriMatcher.match(uri);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        switch (match) {
            case ALARM_ID: {
                String segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);
                count = db.update("alarms", contentValues, "_id=" + rowId, null);
                break;
            }
            default: {
                throw new UnsupportedOperationException(
                        "Cannot update URL: " + uri);
            }
        }
        if (Logger.LOGV) Logger.v("*** notifyChange() rowId: " + rowId + " url " + uri);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}

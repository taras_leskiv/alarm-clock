package com.vk.taras.leskiv.alarm.provider;

import android.provider.BaseColumns;

/**
 * Column definitions for database alarms table
 */
public class AlarmColumns implements BaseColumns {

    /**
     * Hour in 24-hour localtime 0-23.
     * <p>Type: INTEGER</p>
     */
    public static final String HOUR = "hour";

    /**
     * Minutes in localtime 0-59
     * <p>Type: INTEGER</p>
     */
    public static final String MINUTES = "minutes";

    /**
     * Days of week coded as integer
     * <p>Type: INTEGER</>
     */
    public static final String DAYS_OF_WEEK = "daysofweek";

    /**
     * Alarm time in UTC milliseconds from the epoch
     * <p>Type: INTEGER</p>
     */
    public static final String ALARM_TIME = "alarmtime";

    /**
     * True if alarm is active
     * <p>Type: BOOLEAN</p>
     */
    public static final String ENABLED = "enabled";

    /**
     * True if alarm should vibrate
     * <p>Type: BOOLEAN</>
     */
    public static final String VIBRATE = "vibrate";

    /**
     * Message to show when alarm triggers
     * Note: not currently used
     * <p>Type: STRING</p>
     */
    public static final String MESSAGE = "message";

    /**
     * Audio alert to play when alarm triggers
     * <p>Type: STRING</p>
     */
    public static final String ALERT = "alert";

    /**
     * True if alarm item in the list should be expanded
     * <p>Type: BOOLEAN</>
     */
    public static final String EXPANDED = "expanded";

    /**
     * The default sort order for this table
     */
    public static final String DEFAULT_SORT_ORDER = HOUR + ", " + MINUTES + " ASC";

    // Used when filtering enabled alarms
    public static final String WHERE_ENABLED = ENABLED + "=1";

    public static final String[] ALARM_QUERY_COLUMNS = {
            _ID, HOUR, MINUTES, DAYS_OF_WEEK, ALARM_TIME,
            ENABLED, VIBRATE, MESSAGE, ALERT, EXPANDED};

    /**
     * These save calls to corsor.getColumnIndexOrThrow()
     * THEY MUST BE KEPT IN SYNC WITH ABOVE QUERY COLUMNS
     */
    public static final int ALARM_ID_INDEX = 0;
    public static final int ALARM_HOUR_INDEX = 1;
    public static final int ALARM_MINUTES_INDEX = 2;
    public static final int ALARM_DAYS_OF_WEEK_INDEX = 3;
    public static final int ALARM_TIME_INDEX = 4;
    public static final int ALARM_ENABLED_INDEX = 5;
    public static final int ALARM_VIBRATE_INDEX = 6;
    public static final int ALARM_MESSAGE_INDEX = 7;
    public static final int ALARM_ALERT_INDEX = 8;
    public static final int ALARM_EXPANDED_INDEX = 9;
}

/*
 * Copyright 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vk.taras.leskiv.alarm.activity;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.*;
import android.widget.*;
import com.vk.taras.leskiv.alarm.R;
import com.vk.taras.leskiv.alarm.compatutils.ActionBarActivity;
import com.vk.taras.leskiv.alarm.prefs.GeneralSettingsActivity;
import com.vk.taras.leskiv.alarm.prefs.SetAlarmPrefActivity;
import com.vk.taras.leskiv.alarm.provider.Alarm;
import com.vk.taras.leskiv.alarm.provider.Alarms;
import com.vk.taras.leskiv.alarm.utils.Logger;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public class AlarmsActivity extends ActionBarActivity implements TimePickerDialog.OnTimeSetListener, View.OnClickListener {

    public static final String PREFERENCES = "AlarmClock";

    private LayoutInflater mInflater;
    private ListView mAlarmsList;
    private Cursor mCursor;

    // used to update alarms
    private Alarm mCurrentAlarm;

    private String mAm, mPm;

    @Override
    public void onTimeSet(TimePicker timePicker, int newHour, int newMinute) {
        int id = mCurrentAlarm.id;
        boolean enabled = true; // enable alarm if time changed
        Alarm.DaysOfWeek daysOfWeek = mCurrentAlarm.daysOfWeek;
        boolean vibrate = mCurrentAlarm.vibrate;
        String label = mCurrentAlarm.label;
        String alert = mCurrentAlarm.alert == null ? Alarms.ALARM_ALERT_SILENT : mCurrentAlarm.alert.toString();

        SetAlarmPrefActivity.saveAlarm(this, id, enabled, newHour, newMinute, daysOfWeek, vibrate, label, alert, true);
    }


    // Handle all clickable items
    @Override
    public void onClick(View view) {
        View parent;
        Alarm.DaysOfWeek newDaysOfWeek;

        switch (view.getId()) {

            // alarm switch
            case R.id.alarmSwitch:
                parent = (View) view.getParent().getParent();
                boolean isChecked = ((CompoundButton) view).isChecked();
                Alarms.enableAlarm(this, ((Alarm) parent.getTag()).id, isChecked);
            break;

            // clock clicked to change the time
            case R.id.digitalClock:
                // get the top parent (list item) and retrieve alarm object
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                Toast.makeText(AlarmsActivity.this, "id " + mCurrentAlarm.id, Toast.LENGTH_SHORT).show();
                new TimePickerDialog(this, this, mCurrentAlarm.hour, mCurrentAlarm.minutes,
                        DateFormat.is24HourFormat(AlarmsActivity.this)).show();
            break;

            // toggle to expand list item
            case R.id.additional_item_settings:
                // get the top parent (list item) and retrieve alarm object
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                Alarms.setAlarmExpanded(this, mCurrentAlarm.id, !mCurrentAlarm.expanded);
            break;

            case R.id.alarm_item_settings:
                parent = (View) view.getParent().getParent();
                int id = ((Alarm) parent.getTag()).id;
                Intent intent = new Intent(this, SetAlarmPrefActivity.class);
                intent.putExtra(Alarms.ALARM_ID, id);
                startActivity(intent);
            break;


            // DaysOfWeek buttons to toggle days
            case R.id.monday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.MONDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.MONDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;
            case R.id.tuesday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.TUESDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.TUESDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;
            case R.id.wednesday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.WEDNESDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.WEDNESDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;
            case R.id.thursday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.THUSRSDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.THUSRSDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;
            case R.id.friday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.FRIDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.FRIDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;
            case R.id.saturday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.SATURDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.SATURDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;
            case R.id.sunday:
                parent = (View) view.getParent().getParent();
                mCurrentAlarm = (Alarm) parent.getTag();
                newDaysOfWeek = mCurrentAlarm.daysOfWeek;
                newDaysOfWeek.set(Alarm.DaysOfWeek.SUNDAY, !newDaysOfWeek.isSet(Alarm.DaysOfWeek.SUNDAY));
                saveAlarmDaysOfWeek(newDaysOfWeek);
                break;

        }
    }

    private void saveAlarmDaysOfWeek(Alarm.DaysOfWeek daysOfWeek) {

        String alert = mCurrentAlarm.alert == null ? Alarms.ALARM_ALERT_SILENT : mCurrentAlarm.alert.toString();

        SetAlarmPrefActivity.saveAlarm(AlarmsActivity.this, mCurrentAlarm.id,
                mCurrentAlarm.enabled ,
                mCurrentAlarm.hour,
                mCurrentAlarm.minutes,
                daysOfWeek, mCurrentAlarm.vibrate, mCurrentAlarm.label, alert, false);
    }

    private class AlarmsListAdapter extends CursorAdapter {
        public AlarmsListAdapter(Context context, Cursor cursor) {
            super(context, cursor);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View listItem = mInflater.inflate(R.layout.alarm_list_item, parent, false);

            ((TextView) listItem.findViewById(R.id.am)).setText(mAm);
            ((TextView) listItem.findViewById(R.id.pm)).setText(mPm);

            DigitalClockView digitalClockView = (DigitalClockView) listItem.findViewById(R.id.digitalClock);
            digitalClockView.setLive(false);

            return listItem;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final Alarm alarm = new Alarm(cursor);
            view.setTag(alarm);

            // Alarm switch
            CompoundButton alarmSwitch = (CompoundButton) view.findViewById(R.id.alarmSwitch);
            alarmSwitch.setChecked(alarm.enabled);
            alarmSwitch.setOnClickListener(AlarmsActivity.this);

            // Alarm Settings
            ImageButton alarmSettings = (ImageButton) view.findViewById(R.id.alarm_item_settings);
            alarmSettings.setOnClickListener(AlarmsActivity.this);

            // Show additional settings (expand list item)
            final ToggleButton expandItemBtn = (ToggleButton) view.findViewById(R.id.additional_item_settings);
            expandItemBtn.setChecked(alarm.expanded);
            View bottomPart = view.findViewById(R.id.bottom_part_layout);
            if (alarm.expanded) {
                bottomPart.setVisibility(View.VISIBLE);
            } else {
                bottomPart.setVisibility(View.GONE);
            }
            // set day toggle buttons in correct state
            initDaysOfWeekToggles(bottomPart, alarm.daysOfWeek.getBooleanArray());
            expandItemBtn.setTag(bottomPart);
            expandItemBtn.setOnClickListener(AlarmsActivity.this);

            // change the time clicking on the clock
            final DigitalClockView digitalClockView = (DigitalClockView) view.findViewById(R.id.digitalClock);
            digitalClockView.setOnClickListener(AlarmsActivity.this);

            // set the alarm text
            final Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, alarm.hour);
            cal.set(Calendar.MINUTE, alarm.minutes);
            digitalClockView.updateTime(cal);

            // Set the repeat text or leave it blank if it does not repeat.
            TextView tvDaysOfWeek = (TextView) view.findViewById(R.id.daysOfWeek);
            final String daysOfWeekStr = alarm.daysOfWeek.toString(AlarmsActivity.this, false);
            if (daysOfWeekStr != null && daysOfWeekStr.length() != 0) {
                tvDaysOfWeek.setText(daysOfWeekStr);
                tvDaysOfWeek.setVisibility(View.VISIBLE);
            } else {
                tvDaysOfWeek.setVisibility(View.GONE);
            }

            // Display the label
            TextView labelView =
                    (TextView) view.findViewById(R.id.label);
            if (alarm.label != null && alarm.label.length() != 0) {
                labelView.setText(alarm.label);
                labelView.setVisibility(View.VISIBLE);
            } else {
                labelView.setVisibility(View.GONE);
            }
        }
    }

    private void initDaysOfWeekToggles(View parent, boolean[] daysOfWeek) {
        ToggleButton dayToggles[] = new ToggleButton[7];
        dayToggles[0] = (ToggleButton) parent.findViewById(R.id.monday);
        dayToggles[1] = (ToggleButton) parent.findViewById(R.id.tuesday);
        dayToggles[2] = (ToggleButton) parent.findViewById(R.id.wednesday);
        dayToggles[3] = (ToggleButton) parent.findViewById(R.id.thursday);
        dayToggles[4] = (ToggleButton) parent.findViewById(R.id.friday);
        dayToggles[5] = (ToggleButton) parent.findViewById(R.id.saturday);
        dayToggles[6] = (ToggleButton) parent.findViewById(R.id.sunday);

        for (int i = 0; i < 7; i++) {
            dayToggles[i].setOnClickListener(AlarmsActivity.this);
            dayToggles[i].setChecked(daysOfWeek[i]);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] ampm = new DateFormatSymbols().getAmPmStrings();
        mAm = ampm[0];
        mPm = ampm[1];

        mInflater = LayoutInflater.from(this);
        mCursor = Alarms.getAlarmsCursor(getContentResolver());

        updateLayout();
    }

    private void updateLayout() {
        setContentView(R.layout.main);
        mAlarmsList = (ListView) findViewById(R.id.lvAlarms);
        mAlarmsList.setAdapter(new AlarmsListAdapter(this, mCursor));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.alarms_list_menu, menu);

        // Calling super after populating the menu is necessary here to ensure that the
        // action bar helpers have a chance to handle this event.
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_add_alarm:
                Uri uri = Alarms.addAlarm(getContentResolver());
                String segment = uri.getPathSegments().get(1);
                int newId = Integer.parseInt(segment);
                if (Logger.LOGV) {
                    Logger.v("In AlarmClock, new alarm id = " + newId);
                }
                Intent intent = new Intent(this, SetAlarmPrefActivity.class);
                intent.putExtra(Alarms.ALARM_ID, newId);
                startActivity(intent);
                break;

            case R.id.menu_settings:
                startActivity(new Intent(this, GeneralSettingsActivity.class));
                break;

            case android.R.id.home:
                Toast.makeText(this, "Tapped home", Toast.LENGTH_SHORT).show();
                break;

            case R.id.menu_share:
                Toast.makeText(this, "Tapped share", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.vk.taras.leskiv.alarm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.vk.taras.leskiv.alarm.provider.Alarms;
import com.vk.taras.leskiv.alarm.utils.Logger;

/**
 * [READY - DO NOT TOUCH]
 */
public class AlarmInitReceiver extends BroadcastReceiver {

    /**
     * Sets alarm on ACTION_BOOT_COMPLETED.  Resets alarm on
     * TIME_SET, TIMEZONE_CHANGED
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Logger.LOGV) Logger.v("AlarmInitReceiver" + action);

        if (context.getContentResolver() == null) {
            Logger.e("AlarmInitReceiver: FAILURE unable to get content resolver.  Alarms inactive.");
            return;
        }
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            Alarms.saveSnoozeAlert(context, -1, -1);
            Alarms.disableExpiredAlarms(context);
        }
        Alarms.setNextAlert(context);
    }
}


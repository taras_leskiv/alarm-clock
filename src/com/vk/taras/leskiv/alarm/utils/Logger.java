package com.vk.taras.leskiv.alarm.utils;

import com.vk.taras.leskiv.alarm.BuildConfig;

public class Logger {
    public static final String LOG_TAG = "AlarmClock";

    public static final boolean LOGV = BuildConfig.DEBUG;

    public static void v(String logMe) {
        android.util.Log.v(LOG_TAG, logMe);
    }

    public static void e(String logMe) {
        android.util.Log.e(LOG_TAG, logMe);
    }

    public static void e(String logMe, Exception ex) {
        android.util.Log.e(LOG_TAG, logMe, ex);
    }
}

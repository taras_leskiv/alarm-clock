package com.vk.taras.leskiv.alarm.prefs;


import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.*;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TimePicker;
import android.widget.Toast;
import com.vk.taras.leskiv.alarm.BuildConfig;
import com.vk.taras.leskiv.alarm.R;
import com.vk.taras.leskiv.alarm.compatutils.ActionBarPrefActivity;
import com.vk.taras.leskiv.alarm.provider.Alarm;
import com.vk.taras.leskiv.alarm.provider.Alarms;
import com.vk.taras.leskiv.alarm.utils.Logger;
import com.vk.taras.leskiv.alarm.utils.ToastMaster;

/**
 * Manages each alarm
 */
public class SetAlarmPrefActivity extends ActionBarPrefActivity
        implements TimePickerDialog.OnTimeSetListener {

    private static final String PREF_KEY_LABEL = "label";
    private static final String PREF_KEY_TIME = "time";
    private static final String PREF_KEY_ALARM = "alarm";
    private static final String PREF_KEY_VIBRATE = "vibrate";
    private static final String PREF_KEY_REPEAT = "setRepeat";

    private EditTextPreference mLabel;
    private Preference mTimePreference;
    private AlarmPreference mAlarmPreference;
    private CheckBoxPreference mVibratePreference;
    private RepeatPreference mRepeatPreference;

    private MenuItem mDeleteAlarmItem;
    private MenuItem mTestAlarmItem;

    private int mId;
    private boolean mEnabled;
    private int mHour;
    private int mMinutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.alarm_prefs);

        mLabel = (EditTextPreference) findPreference(PREF_KEY_LABEL);
        mLabel.setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference p, Object newValue) {
                        p.setSummary((String) newValue);
                        return true;
                    }
                }
        );
        mTimePreference = findPreference(PREF_KEY_TIME);
        mAlarmPreference = (AlarmPreference) findPreference(PREF_KEY_ALARM);
        mVibratePreference = (CheckBoxPreference) findPreference(PREF_KEY_VIBRATE);
        mRepeatPreference = (RepeatPreference) findPreference(PREF_KEY_REPEAT);

        Intent intent = getIntent();
        mId = intent.getIntExtra(Alarms.ALARM_ID, -1);
        if (Logger.LOGV) {
            Logger.v("In SetAlarmPrefActivity, alarm id = " + mId);
        }

        // load alarm details from database
        Alarm alarm = Alarms.getAlarm(getContentResolver(), mId);
        if (alarm == null) {
            finish();
            return;
        }

        mEnabled = alarm.enabled;
        mLabel.setText(alarm.label);
        mLabel.setSummary(alarm.label);
        mHour = alarm.hour;
        mMinutes = alarm.minutes;
        mRepeatPreference.setDaysOfWeek(alarm.daysOfWeek);
        mVibratePreference.setChecked(alarm.vibrate);
        // Give the alert uri to the preference
        mAlarmPreference.setAlert(alarm.alert);
        updateTime();
    }

    private void saveAlarm() {
        final String alert = mAlarmPreference.getAlertString();
        long time = Alarms.setAlarm(this, mId, mEnabled, mHour, mMinutes,
                mRepeatPreference.getDaysOfWeek(), mVibratePreference.isChecked(), mLabel.getText(), alert);
        if (mEnabled) {
            popAlarmSetToast(this, time);
        }
    }

    /**
     * Write alarm out to persistent store and pops toast if alarm
     * enabled.
     * Used only in test code.
     */
    public static void saveAlarm(
            Context context, int id, boolean enabled, int hour, int minute,
            Alarm.DaysOfWeek daysOfWeek, boolean vibrate, String label,
            String alert, boolean popToast) {
        if (Logger.LOGV) Logger.v("** saveAlarm " + id + " " + label + " " + enabled
                + " " + hour + " " + minute + " vibe " + vibrate);

        // Fix alert string first
        long time = Alarms.setAlarm(context, id, enabled, hour, minute,
                daysOfWeek, vibrate, label, alert);

        if (enabled && popToast) {
            popAlarmSetToast(context, time);
        }
    }

    @Override
    public void onBackPressed() {
//        saveAlarm();  // Uncomment for build
        finish();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mTimePreference) {
            new TimePickerDialog(this, this, mHour, mMinutes,
                    DateFormat.is24HourFormat(this)).show();
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        mHour = hourOfDay;
        mMinutes = minute;
        updateTime();
        // If the time has been changed enable the alarm
        mEnabled = true;
    }

    private void updateTime() {
        if (Logger.LOGV) {
            Logger.v("updateTime " + mId);
        }
        mTimePreference.setSummary(Alarms.formatTime(this, mHour, mMinutes,
                mRepeatPreference.getDaysOfWeek()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.set_alarm_menu, menu);

        if (BuildConfig.DEBUG) {
            mTestAlarmItem = menu.add(0, 0, 0, "Test Alarm");
        }

        // Calling super after populating the menu is necessary here to ensure that the
        // action bar helpers have a chance to handle this event.
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_cancel_editing_alarm:
                finish();
                return true;

            case R.id.menu_save_alarm:
                mEnabled = true;
                saveAlarm();
                finish();
                return true;

            case R.id.menu_delete_alarm:
                Alarms.deleteAlarm(this, mId);
                finish();
                return true;
        }

        if (BuildConfig.DEBUG) {
            if (item == mTestAlarmItem) {
                setTestAlarm();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display a toast that tells the user how long until the alarm
     * goes off.  This helps prevent "am/pm" mistakes.
     */
    static void popAlarmSetToast(Context context, int hour, int minute,
                                 Alarm.DaysOfWeek daysOfWeek) {
        popAlarmSetToast(context,
                Alarms.calculateAlarm(hour, minute, daysOfWeek)
                        .getTimeInMillis());
    }

    private static void popAlarmSetToast(Context context, long timeInMillis) {
        String toastText = formatToast(context, timeInMillis);
        Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_LONG);
        ToastMaster.setToast(toast);
        toast.show();
    }

    /**
     * format "Alarm set for 2 days 7 hours and 53 minutes from
     * now"
     */
    static String formatToast(Context context, long timeInMillis) {
        long delta = timeInMillis - System.currentTimeMillis();
        long hours = delta / (1000 * 60 * 60);
        long minutes = delta / (1000 * 60) % 60;
        long days = hours / 24;
        hours = hours % 24;

        String daySeq = (days == 0) ? "" :
                (days == 1) ? context.getString(R.string.day) :
                        context.getString(R.string.days, Long.toString(days));

        String minSeq = (minutes == 0) ? "" :
                (minutes == 1) ? context.getString(R.string.minute) :
                        context.getString(R.string.minutes, Long.toString(minutes));

        String hourSeq = (hours == 0) ? "" :
                (hours == 1) ? context.getString(R.string.hour) :
                        context.getString(R.string.hours, Long.toString(hours));

        boolean dispDays = days > 0;
        boolean dispHour = hours > 0;
        boolean dispMinute = minutes > 0;

        int index = (dispDays ? 1 : 0) |
                (dispHour ? 2 : 0) |
                (dispMinute ? 4 : 0);

        String[] formats = context.getResources().getStringArray(R.array.alarm_set);
        return String.format(formats[index], daySeq, hourSeq, minSeq);
    }

    /**
     * Test code: this is disabled for production build.  Sets
     * this alarm to go off on the next minute
     */
    void setTestAlarm() {

        // start with now
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());

        int nowHour = c.get(java.util.Calendar.HOUR_OF_DAY);
        int nowMinute = c.get(java.util.Calendar.MINUTE);

        int minutes = (nowMinute + 1) % 60;
        int hour = nowHour + (nowMinute == 0 ? 1 : 0);

        saveAlarm(this, mId, true, hour, minutes, mRepeatPreference.getDaysOfWeek(),
                true, mLabel.getText(), mAlarmPreference.getAlertString(), true);
    }

}
